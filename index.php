<?php
    require_once 'core/functions.php';

    if (isAuthorize()) {
        header('Location: admin.php');
        die;
    }

    if (isGuest()) {
        header('Location: list.php');
        die;
    }

    $error = '';
    if (isPost()) {
        switch ($_POST['action']) {
            case 'guest':
                $_SESSION['name'] = getParameterPost('name');
                header('Location: list.php');
                die;
                break;
            case 'login':
                $isCaptcha = isCaptcha();
                if ($isCaptcha && getParameterPost('captcha') != getParameterSession('captcha')) {
                    $error = 'Неправильно ввели текст с картинки.';
                } else {
                    if (login(getParameterPost('login'), getParameterPost('password'))) {
                        clearBlackList();
                        header('Location: admin.php');
                        die;
                    } else {
                        addCountError($isCaptcha);
                        $error = 'Ошибка авторизации!';
                    }
                }
                break;
            default:
                break;
        }
    }


    if (isBlackList()) {
        echo 'Подождите часок, мы вас заблокировали, а затем попробуйте снова!';
        die;
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Авторизация</title>
        <style  type="text/css">
            .form-signin {
                max-width: 400px;
                min-height: 190px;
                padding: 19px 29px 29px;
                margin: 100px auto auto 20px;
                background-color: #fff;
                border: 1px solid #e5e5e5;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
            }
            .form-signin input, .form-signin button {
                margin-bottom: 10px;
            }
            .red {
                color: red;
            }
            .captcha {
                margin-bottom:20px;
                margin-top:20px;
            }
        </style>
        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <form class="form-signin" role="form" method="post">
                    <input name="name" type="text" class="form-control" placeholder="Имя" required autofocus>
                    <button class="btn btn-lg btn-primary" name="action" value="guest" type="submit">Войти как гость</button>
                </form>
            </div>
            <div class="col-md-3">
                <form class="form-signin" role="form" method="post">
                    <input class="form-control" name="login" type="text" placeholder="Логин" required autofocus>
                    <input class="form-control"  name="password" type="password" placeholder="Пароль" required>
                    <?php if(isCaptcha()): createCaptcha();?>
                    <div style="text-align:center;">
                        <h5>Введите текст, который видите на картинке</h5>
                        <div class="captcha">
                            <img style="padding-bottom: 10px;" src="img/captcha.png">
                            <input class="form-control" type="text"  name="captcha" required>
                        </div>
                    </div>
                    <?php endif; ?>
                    <span class="red"><?php echo !empty($error) ? $error : ''; ?></span>
                    <button class="btn btn-lg btn-primary" name="action" value="login" type="submit">Авторизация</button>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>
    </body>
</html>