<?php
    require_once 'core/functions.php';

    if (!isAuthorize() && !isGuest()) {
        header('HTTP/1.1 403 Forbidden');
        echo 'Данная страница закрыта';
        die;
    }

    $fileFolder = __DIR__ . '/tests/';
    if (!file_exists($fileFolder . $_GET['fileName'])) {
        header("HTTP/1.0 404 Not Found");
        $page404 = file_get_contents(__DIR__ . '/404.html');
        echo $page404;
        exit;
    }

    if (empty($_GET['fileName'])) {
        echo 'Непредвиденная ошибка';
        exit;
    }

    $tests = json_decode(file_get_contents($fileFolder . $_GET['fileName']), true);

    if (empty($tests)) {
        echo 'Непредвиденная ошибк';
        exit;
    }

    if (!empty($_POST['variant'])) {
        $correct = $error = 0;
        foreach ($_POST['variant'] as $key => $variant) {
            if ($tests[$key]['correct'] == $variant) {
                $correct++;
            } else {
                $error++;
            }
        }
        createCertificate($correct, $error);
    }

    function createCertificate($correct, $error)
    {
        $font = 'font/a_AvanteBs.ttf';
        header("Content-type: image/png");
        //header('Content-Disposition: attachment; filename="certificate.png"');
        $name = getParameterSession('name');
        $string = "Выдан на имя: $name";
        $all = $correct+$error;
        $string1 = "Ваша оценка: $correct/$all";
        $im     = imagecreatefrompng("img/certificate.png");
        $orange = imagecolorallocate($im, 220, 210, 60);
        $px     = (imagesx($im) - 17.5 * strlen($string)) / 2;
        imagefttext($im, 43, 0, $px, 429, $orange, $font, $string);
        imagefttext($im, 43, 0, $px, 529, $orange, $font, $string1);
        imagepng($im);
        imagedestroy($im);
    }

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset=utf-8">
    <title>Тест</title>
    <style type="text/css">
        body {
            margin: 0px;
        }
        #menu {
            background: burlywood;
            height: 30px;
            padding: 20px 0px 0px 20px;
        }
        #menu a {
            text-decoration: none;
            margin-right: 10px;
        }
        #container {
            margin-left: 20px;
        }
    </style>
</head>
    <body>
        <div id="menu">
            <?php foreach (getMenu() as $menu): ?>
                <a class="menu" href="<?php echo $menu['url'] ?>"><?php echo $menu['name'] ?></a>
            <?php endforeach; ?>
        </div>

        <div id="container">
            <?php if (!isset($result)): ?>
            <div>
                <h3>Тест</h3>
                <form enctype="multipart/form-data" method="post" name="testForm">
                    <?php foreach ($tests as $i => $test): ?>
                        <h4><?php echo $test['question']; ?></h4>
                        <?php foreach ($test['variants'] as $key => $variant): ?>
                            <input required type="radio" name="variant[<?php echo $i; ?>]" value="<?php echo $key; ?>">
                            <span><?php echo $variant; ?></span><br>
                        <?php endforeach; endforeach; ?>
                    <input type="submit" value="Отправить">
                </form>
            </div>

            <?php else: ?>
                <div>
                    Тест пройден.<br>
                    Правильных ответов - <?php echo $result['correct']; ?>;<br>
                    Неправильных ответов - <?php echo $result['error']; ?>.
                </div>
            <?php endif; ?>
        </div>
    </body>
</html>
