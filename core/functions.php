<?php

session_start();

function isPost()
{
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        return true;
    } else {
        return false;
    }
}

function getParameterPost($property)
{
    if (!empty($_POST[$property])) {
        return $_POST[$property];
    }
    return '';
}

function getParameterSession($property)
{
    if (!empty($_SESSION[$property])) {
        return $_SESSION[$property];
    }
    return '';
}

function setParameterSession($property, $value)
{
   $_SESSION[$property] = $value;
}

function login($login, $password)
{
    if (empty($login) || empty($password)) {
        return false;
    }

    if (!file_exists('users/'. $login . '.json')) {
        return false;
    }

    $data = json_decode(file_get_contents('users/'. $login . '.json'), true);
    if (empty($data['password'])) {
        return false;
    }

    if ($data['password'] == md5($password)) {
        setParameterSession('name', $data['name']);
        setParameterSession('login', 1);
        return true;
    }

    return false;
}

function isAuthorize()
{
    if (!empty(getParameterSession('login'))) {
        return true;
    }
    return false;
}

function isGuest()
{
    if (empty(getParameterSession('login')) && !empty(getParameterSession('name'))) {
        return true;
    }
    return false;
}

function logout()
{
    session_destroy();
}

function getMenu()
{
    $menu = array();
    if (isAuthorize()) {
        $menu[] = ['name' => 'Добавить тест', 'url' => 'admin.php'];
    }
    $menu[] = ['name' => 'Список тестов', 'url' => 'list.php'];
    $menu[] = ['name' => 'Выйти', 'url' => 'logout.php'];
    return $menu;
}

function getIp()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function addCountError($isCaptcha)
{
    if ($isCaptcha) {
        $type = 'captchaCount';
    } else {
        $type = 'countError';
    }

    $error = getBlacklist();
    $ip = getIp();

    if (empty($error[$ip][$type])) {
        $error[$ip][$type] = 1;
    } else {
        $error[$ip][$type]++;
    }

    setBlacklist($error);
    addBlackList();
}

function isCaptcha()
{
    $error = getBlacklist();
    $ip = getIp();

    if (empty($error[$ip]['countError'])) {
        return false;
    }

    if ($error[$ip]['countError'] > 6) {
        return true;
    }

    return false;
}

function createCaptcha()
{
    $numberCaptcha = rand(100000,9999999);
    $image = imagecreatetruecolor(170, 50);
    $background_color = imagecolorallocate($image, 235, 250, 255);
    imagefilledrectangle($image,0,0,200,50,$background_color);
    $text_color = imagecolorallocate($image, 0,0,0);
    imagestring($image, 5, 50, 16, $numberCaptcha, $text_color);
    imagepng($image, "img/captcha.png");
    setParameterSession('captcha', $numberCaptcha);
}

function addBlackList()
{
    $blacklist = getBlacklist();
    $ip = getIp();

    if (!empty($blacklist[$ip]['captchaCount']) && $blacklist[$ip]['captchaCount'] > 5) {
        $blacklist[$ip]['blackList'] = time();
        unset($blacklist[$ip]['countError'], $blacklist[$ip]['captchaCount']);
        setBlacklist($blacklist);
    }
}

function isBlackList()
{
    $blacklist = getBlacklist();
    $ip = getIp();

    if (empty($blacklist[$ip]['blackList'])) {
        return false;
    }

    if ((time() - $blacklist[$ip]['blackList']) < 3600) {
        return true;
    } else {
        clearBlackList();
        setBlacklist($blacklist);
        return false;
    }
}

function getBlacklist()
{
    return json_decode(file_get_contents('error/blacklist.json'), true);
}

function setBlacklist($blacklist)
{
    file_put_contents('error/blacklist.json', json_encode($blacklist));
}

function clearBlackList()
{
    $blackList = getBlacklist();
    $ip = getIp();
    unset($blackList[$ip]);
    setBlacklist($blackList);
}