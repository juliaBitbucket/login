<?php
require_once 'core/functions.php';

if (empty($_GET['fileName'])) {
    return;
}

$error = '';
if (unlink('tests/' . $_GET['fileName'])) {
    $error = 'Тест удалён успешно!';
} else {
    $error = 'Не удалось удалить тест.';
}
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset=utf-8">
        <title>Удаление тестов</title>
        <style type="text/css">
            body {
                margin: 0px;
            }
            #menu {
                background: burlywood;
                height: 30px;
                padding: 20px 0px 0px 20px;
            }
            #menu a {
                text-decoration: none;
                margin-right: 10px;
            }
        </style>
    </head>
    <body>
        <div id="menu">
            <?php foreach (getMenu() as $menu): ?>
                <a class="menu" href="<?php echo $menu['url'] ?>"><?php echo $menu['name'] ?></a>
             <?php endforeach; ?>
        </div>
    <span><?php echo $error; ?></span>
    </body>
</html>